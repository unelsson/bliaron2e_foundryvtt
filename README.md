Official Bliaron 2nd Edition system support for Foundry VTT.

Changelog 0.5.0
- updated to Foundry VTT version 10 (compatibility, API updates, breaking changes)

Changelog 0.4.1
- updated to Foundry VTT version 9 (compatibility)
- Basic character sheet
- Character sheet feature: Click the attribute for the dice roll
- Magic Qualities and Effects
- Basic support for items and perks
- Map of Bliaron
- Token art assets

In Bliaron, you play as ordinary people with magical talent in a world where magic is extremely powerful and strictly controlled. The first nations are emerging and taking over native tribal peoples, while power-hungry organizations are looking for recruits for their operations. The Grand Sahen controls the politics of the Republic of Bliwon, while the Blackhands are plotting under resurrected god-king Artan in the masculine North.

Content license CC-BY-NC-SA

Bliaron 2nd Edition
https://www.drivethrurpg.com/product/311966/Bliaron-2nd-Edition

Bliaron 2nd Edition - free Quick Start Introductory Rulebook
https://www.drivethrurpg.com/product/328770/Bliaron-2nd-Edition--Quick-Start-Introductory-Rulebook

Author: Uoti Huotari / unelsson
