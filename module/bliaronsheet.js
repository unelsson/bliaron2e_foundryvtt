export class BliaronSheet extends ActorSheet {
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["sheet", "actor"],
            template: "systems/bliaron2e/templates/actorsheet.html",
            width: 500,
            height: 800
            });
    }

    getData(options) {
        let baseData = super.getData(options);
        let sheetData = {};
        sheetData = Object.assign({}, baseData.document);
        let actorData = baseData.document.system;
        sheetData.actor = Object.assign({}, actorData);
        let equipments = [];
        let magics = [];
        let perks = [];
        for (let i of baseData.items) {
            if (i.type === "weapon" ||
                i.type === "armor" ||
                i.type === "generic") {
                    equipments.push(i);
            }
            else if (i.type === "magic") {
                magics.push(i);
            }
            else if (i.type === "perk") {
                perks.push(i);
            }
        }        
        sheetData.actor.equipments = equipments;
        sheetData.actor.magics = magics;
        sheetData.actor.perks = perks;
        console.log (sheetData);
        return sheetData;
    }

    activateListeners(html) {
        super.activateListeners(html);

        html.find('.rollable').click(this._onRoll.bind(this));

        if (!super.isEditable) return;

        html.find('.item-delete').click(ev => {
            const li = $(ev.currentTarget).parents(".item");
            const item = this.actor.items.get(li.data("itemId"));
            item.delete();
            li.slideUp(200, () => this.render(false));
        });

        html.find('.item-edit').click(ev => {
            const li = $(ev.currentTarget).parents(".item");
            const item = this.actor.items.get(li.data("itemId"));
            item.sheet.render(true);
        });

    }

    _onRoll(event) {
        event.preventDefault();
        const element = event.currentTarget;
        const dataset = element.dataset;

        if (dataset.roll) {
            let roll = new Roll(dataset.roll, this.actor);
            let label = dataset.label ? `Rolling ${dataset.label}` : '';
            roll.toMessage({
            speaker: ChatMessage.getSpeaker({ actor: this.actor }),
            flavor: label
            });
        }
    }
}
