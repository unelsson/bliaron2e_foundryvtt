export class BliaronItemSheet extends ItemSheet {
    static get defaultOptions() {
        return mergeObject(super.defaultOptions, {
            classes: ["sheet", "item"],
            template: "systems/bliaron2e/templates/itemsheet.html",
            width: 300,
            height: 400
        });
    }

    async getData(options) {
        const superData = await super.getData(options);
        const data = await Object.assign({}, superData.document);
        data.enrichedDescription = await TextEditor.enrichHTML(this.object.system.description, {async: true});
        return data;
    }

    activateListeners(html) {
        super.activateListeners(html);

        // Everything below here is only needed if the sheet is editable
        if (!this.isEditable) return;

      // Roll handlers, click handlers, etc. would go here.
    }
}
