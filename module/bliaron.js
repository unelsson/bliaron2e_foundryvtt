/**
 * System: Bliaron 2nd Edition
 * Author: Uoti Huotari
 * Software License: MIT
 * Content License: CC-BY-NC-SA
 */

import { BliaronActor } from "./bliaronactor.js";
import { BliaronSheet } from "./bliaronsheet.js";
import { BliaronItemSheet } from "./bliaronitemsheet.js";

Hooks.once('init', async function () {
    console.log(`Bliaron | Initializing Bliaron 2nd Edition`);

    //TODO: set CONFIG. "show dice rolls"
    //Do we need to work on initiatives or combat?

    Actors.unregisterSheet('core', ActorSheet);
    Actors.registerSheet('bliaron2e', BliaronSheet, { makeDefault: true });
    //TODO: Make different types of charsheets: Actors.registerSheet('bliaron2e', BliaronSheet, { types: ["character", "creature", "spirit"], makeDefault: true });

    Items.unregisterSheet('core', ItemSheet);
    Items.registerSheet('bliaron2e', BliaronItemSheet, { makeDefault: true});

});
